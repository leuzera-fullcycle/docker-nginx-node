const express = require('express');
const app = express()
const port = 3000

const { Client } = require('pg');
const dbConfig = {
    host: 'db',
    user: 'postgres',
    password: 'fullcycle123',
    database: 'app',
    port: 5432,
}

createTable();

app.get('/', async (req, res) => {
    let html = '<h1>Full Cycle Rocks!</h1>'

    await createRandomPerson()
    html = await getPeople(html)

    res.send(html)
})

app.listen(port, () => {
    console.info("Rodando na porta " + port)
})

async function createTable() {
    console.log("Criando tabela.")
    const createTables = 'CREATE TABLE IF NOT EXISTS people (id integer primary key generated always as identity, nome varchar(255));';
    const client = new Client(dbConfig);

    try {
        await client.connect()
    } catch (error) {
        console.error(error);
        return
    }

    try {
        await client.query(createTables)
        console.info('Tabela criada com sucesso.');
    } catch (error) {
        console.error(error);
    } finally {
        client.end();
    }
}

async function createRandomPerson() {
    const nome = generateRandom()
    const query = "INSERT INTO people(nome) VALUES('" + nome + "')"
    const client = new Client(dbConfig);

    try {
        await client.connect()
    } catch (error) {
        console.error(error);
        return
    }

    try {
        await client.query(query)
    } catch (error) {
        console.error(error);
    } finally {
        client.end();
    }
}

async function getPeople(html) {
    const query = 'SELECT * from people;';
    const client = new Client(dbConfig);

    try {
        await client.connect()
    } catch (error) {
        console.error(error);
        return
    }

    try {
        const peoples = await client.query(query)

        if (peoples.rows) {
            html += '\n\n';
            html += '<ul>';
            for (const people of peoples.rows) {
                html += '\n<li>[' + people.id + '] ' + people.nome + '</li>';
            }
            html += '</ul>';
        }

    } catch (error) {
        console.error(error);
    } finally {
        client.end();
    }

    return html;
}

function generateRandom() {
    const nomes = ["Laila", "Rebeca", "Gabriel", "Daniel", "José", "Lara", "Iara", "Arthur", "Álvaro", "Maurício",
        "Amanda", "Jade", "Heitor", "Davi Lucca", "Kelvin"]
    const random = Math.floor(Math.random() * 14);

    return nomes[random]
}